#include "board.hpp"


#include <vector> 
#include <sstream> 




Board::Board(string fen){
    vector<string> infos;
    for (int i = 0;i<64;i++){
        Square[i] = 0;
    }
    stringstream ss(fen);
    string s;
    while(getline(ss,s,' ')){
        infos.push_back(s);
    }
    string position = infos[0];
    string color = infos[1];
    string rocks = infos[2];
    string en_passants = infos[3];

    half = stoi(infos[4]);
    moves = stoi(infos[5]);
    int id = 0;
    for (string::size_type i=0;i<position.size();i++){
        char c = position[i];


        switch (c){
        case 'k':
            Square[id] = Piece::King | Piece::Black;
            break;
        case 'p':
            Square[id] = Piece::Pawn | Piece::Black;
            break;
        case 'n':
            Square[id] = Piece::Knight | Piece::Black;
            break;
        case 'b':
            Square[id] = Piece::Bishop | Piece::Black;
            break;
        case 'r':
            Square[id] = Piece::Rook | Piece::Black;
            break;
        case 'q':
            Square[id] = Piece::Queen | Piece::Black;
            break;
        case 'K':
            Square[id] = Piece::King | Piece::White;
            break;
        case 'P':
            Square[id] = Piece::Pawn | Piece::White;
            break;
        case 'N':
            Square[id] = Piece::Knight | Piece::White;
            break;
        case 'B':
            Square[id] = Piece::Bishop | Piece::White;
            break;
        case 'R':
            Square[id] = Piece::Rook | Piece::White;
            break;
        case 'Q':
            Square[id] = Piece::Queen | Piece::White;
            break;
        case '/':
            id--;
            break;
        
        default:
            id+=stoi((string){c,'\0'})-1;
            break;
        }
        id++;
    }
    whiteToMove = (color == "w");
}