#include "piece.hpp"

#include <string>

using namespace std;

class Board {
    public:
    int Square[64];
    bool whiteToMove;
    int half,moves;
    Board(string fen);
};