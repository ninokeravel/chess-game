#include <iostream>
#include <SFML/Graphics.hpp>

#include "board.hpp"

#define SSIZE 80
#define BSIZE 8*SSIZE

using namespace std;


sf::Color const White( 255, 255, 181,255);
sf::Color const Black( 61, 0, 0,255);
sf::Texture piecesTextures[(Piece::Queen | Piece::Black) + 1];



int index(int i,int j){
    return i*8+j;
}

void create_squares(sf::RectangleShape squares[64]){
    for (int i = 0;i<8;i++){
        for(int j = 0;j<8;j++){
            squares[index(i,j)] = sf::RectangleShape(sf::Vector2f(SSIZE,SSIZE));
            squares[index(i,j)].setPosition(sf::Vector2f(SSIZE*j,SSIZE*i));
            if ((i+j)%2 == 0){
                squares[index(i,j)].setFillColor(Black);
            } else {
                squares[index(i,j)].setFillColor(White);
            }
        }
    }
}

void render_board(sf::RenderWindow *window,sf::RectangleShape squares[64],Board board){
    sf::Sprite sprite;
    for (int i=0;i<64;i++){
        window->draw(squares[i]);
        if (board.Square[i]){
            sprite.setTexture(piecesTextures[board.Square[i]]);
            sprite.setPosition(sf::Vector2f(SSIZE*(i%8),SSIZE*(i/8)));
            sprite.move(sf::Vector2f(SSIZE/2-30,SSIZE/2-30));
            window->draw(sprite);
        }
    }
}

int main(){
    string fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    Board board = Board(fen);
    sf::RenderWindow window(sf::VideoMode(BSIZE,BSIZE),"Game");
    
    //create background
    sf::RectangleShape squares[64];
    create_squares(squares);

    //create pieces image colection
    for (int piece = 1; piece<7; piece++){
        for (int color = 8; color <= 16; color+=8){
            piecesTextures[piece | color].loadFromFile("textures/"+to_string(piece|color)+".png");
        }
    }

    while (window.isOpen())
    {
        // managing the window
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        render_board(&window,squares,board);
        window.display();
    }
    return 0;
}